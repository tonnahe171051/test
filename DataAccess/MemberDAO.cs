﻿using BusinessObject;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataAccess
{

    public class MemberDAO
    {
        private static List<MemberObject> memberList = new List<MemberObject>()
        {
            new MemberObject(11,"Mohammed Sati","sati@gmail.com", "123", "Riyard","Saudi Arabia"),
            new MemberObject(2,"Penaldo","pendo@gmail.com", "123", "Seoul","South Korea"),
            new MemberObject(3,"Mohammad Liem","liem@gmail.com", "123", "Libon","Portugal"),
            new MemberObject(4,"Hoi Pen","hoipen@gmail.com", "123", "Paris","France"),
            new MemberObject(5,"Tron Thue","hidetax@gmail.com", "123", "New York","US"),
            new MemberObject(6,"Vap Co","vapco@gmail.com", "123", "Manchester","US"),
            new MemberObject(7,"Yuri Gagarin","space@gmail.com", "123", "Leningrad","USSR"),
            new MemberObject(8,"Xi Jinping","xipool@gmail.com", "123", "Beijing","China"),
            new MemberObject(9,"Le Hong Quang","quanglh@gmail.com", "123", "Phu Tho","VietNam"),
            new MemberObject(10,"Kim Jong Un","cutenuke@gmail.com", "123", "Pyong Yang","North Korea"),
            new MemberObject(12,"Cam On Bau Duc","thankbd@gmail.com", "123", "Gia Lai","VietNam")
        };

        private static MemberDAO instance = null;
        private static readonly object instanceLock = new object();
        private MemberDAO()
        {
            IConfigurationRoot configurationRoot = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            string adminEmail = configurationRoot["admin:email"];
            string adminPassword = configurationRoot["admin:password"];
            memberList.Add(new MemberObject(1, "Admin", adminEmail, adminPassword, "Ha Noi", "VietNam"));
        }
        public static MemberDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new MemberDAO();
                    }
                    return instance;
                }
            }
        }
        //using LinQ
        public List<MemberObject> GetListAllMember(bool descending = false)
        {
            var query = descending ? memberList.OrderByDescending(mb => mb.MemberName) : memberList.OrderBy(mb => mb.MemberName);
            return query.ToList();
        }
        //using LinQ
        public MemberObject GetMemberByEmailPassword(string email, string password)
        {
            var query = from mb in memberList
                        where mb.Email.Equals(email) && mb.Password.Equals(password)
                        select mb;

            return query.SingleOrDefault();
        }
        public MemberObject GetMemberByID(int id)
        {

            return memberList.SingleOrDefault(member => member.MemberID == id);
        }

        public void InsertMember(MemberObject member)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(member.Email);
            if (GetMemberByID(member.MemberID) == null && member.Email.Trim() != "" && member.Password.Trim() != "" && match.Success)
            {
                memberList.Add(member);
                return;
            }
            else
            {
                throw new Exception("Member existed!");
            }
        }

        public void UpdateMember(MemberObject Umember)
        {
            MemberObject member = GetMemberByID(Umember.MemberID);
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(member.Email);
            if (member != null && member.Email.Trim() != "" && member.Password.Trim() != "" && match.Success)
            {
                member.MemberName = Umember.MemberName;
                member.Email = Umember.Email;
                member.Password = Umember.Password;
                member.City = Umember.City;
                member.Country = Umember.Country;
            }
            else
            {
                throw new Exception("Can not update, please check information format!");
            }

        }

        public void DeleteMember(int id)
        {
            MemberObject member = GetMemberByID(id);
            if (member != null)
            {
                memberList.Remove(member);
            }
            else
            {
                throw new Exception("Member did not existed!");
            }
        }
        //using LinQ
        public List<MemberObject> searchMemberByName(String name, List<MemberObject> list)
        {
            if (list == null)
            {
                list = memberList;
            }
            var query = from mb in list
                        where mb.MemberName.ToString().Contains(name.ToString())
                        select mb;
            return query.ToList();
        }
        //using LinQ
        public List<MemberObject> searchMemberById(int id, List<MemberObject> list)
        {
            if (list == null)
            {
                list = memberList;
            }
            var query = from mb in list
                        where mb.MemberID.ToString().Equals(id.ToString())
                        select mb;
            return query.ToList();
        }
        //using LinQ
        public List<MemberObject> filterByCity(String city, List<MemberObject> list)
        {
            if (list == null)
            {
                list = memberList;
            }
            var query = from mb in list
                        where mb.City.Equals(city)
                        select mb;
            return query.ToList();
        }
        //using LinQ
        public List<MemberObject> filterByCountry(String Country, List<MemberObject> list)
        {
            if (list == null)
            {
                list = memberList;
            }
            var query = from mb in list
                        where mb.Country.Equals(Country)
                        select mb;
            return query.ToList();
        }


    }
}
