﻿namespace MyStoreWinApp
{
    partial class frmMemberDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMemberDetails));
            lbMemberID = new Label();
            lbMemberName = new Label();
            lbCity = new Label();
            lbEmail = new Label();
            lbPassword = new Label();
            lbCountry = new Label();
            txtMemberName = new TextBox();
            txtMemberID = new TextBox();
            txtCity = new TextBox();
            txtPassword = new TextBox();
            txtEmail = new TextBox();
            txtCountry = new TextBox();
            btnSave = new Button();
            btnCancel = new Button();
            btnShowPassword = new Button();
            SuspendLayout();
            // 
            // lbMemberID
            // 
            lbMemberID.AutoSize = true;
            lbMemberID.Location = new Point(71, 67);
            lbMemberID.Name = "lbMemberID";
            lbMemberID.Size = new Size(80, 20);
            lbMemberID.TabIndex = 0;
            lbMemberID.Text = "MemberID";
            lbMemberID.Click += label1_Click;
            // 
            // lbMemberName
            // 
            lbMemberName.AutoSize = true;
            lbMemberName.Location = new Point(71, 161);
            lbMemberName.Name = "lbMemberName";
            lbMemberName.Size = new Size(109, 20);
            lbMemberName.TabIndex = 1;
            lbMemberName.Text = "Member Name";
            // 
            // lbCity
            // 
            lbCity.AutoSize = true;
            lbCity.Location = new Point(71, 269);
            lbCity.Name = "lbCity";
            lbCity.Size = new Size(34, 20);
            lbCity.TabIndex = 2;
            lbCity.Text = "City";
            // 
            // lbEmail
            // 
            lbEmail.AutoSize = true;
            lbEmail.Location = new Point(431, 67);
            lbEmail.Name = "lbEmail";
            lbEmail.Size = new Size(46, 20);
            lbEmail.TabIndex = 3;
            lbEmail.Text = "Email";
            // 
            // lbPassword
            // 
            lbPassword.AutoSize = true;
            lbPassword.Location = new Point(431, 161);
            lbPassword.Name = "lbPassword";
            lbPassword.Size = new Size(70, 20);
            lbPassword.TabIndex = 4;
            lbPassword.Text = "Password";
            // 
            // lbCountry
            // 
            lbCountry.AutoSize = true;
            lbCountry.Location = new Point(431, 269);
            lbCountry.Name = "lbCountry";
            lbCountry.Size = new Size(60, 20);
            lbCountry.TabIndex = 5;
            lbCountry.Text = "Country";
            // 
            // txtMemberName
            // 
            txtMemberName.Location = new Point(186, 154);
            txtMemberName.Name = "txtMemberName";
            txtMemberName.Size = new Size(225, 27);
            txtMemberName.TabIndex = 6;
            // 
            // txtMemberID
            // 
            txtMemberID.Location = new Point(186, 60);
            txtMemberID.Name = "txtMemberID";
            txtMemberID.Size = new Size(225, 27);
            txtMemberID.TabIndex = 7;
            // 
            // txtCity
            // 
            txtCity.Location = new Point(186, 262);
            txtCity.Name = "txtCity";
            txtCity.Size = new Size(225, 27);
            txtCity.TabIndex = 8;
            // 
            // txtPassword
            // 
            txtPassword.Location = new Point(517, 154);
            txtPassword.Name = "txtPassword";
            txtPassword.Size = new Size(225, 27);
            txtPassword.TabIndex = 9;
            // 
            // txtEmail
            // 
            txtEmail.Location = new Point(517, 60);
            txtEmail.Name = "txtEmail";
            txtEmail.Size = new Size(225, 27);
            txtEmail.TabIndex = 10;
            // 
            // txtCountry
            // 
            txtCountry.Location = new Point(517, 262);
            txtCountry.Name = "txtCountry";
            txtCountry.Size = new Size(225, 27);
            txtCountry.TabIndex = 11;
            // 
            // btnSave
            // 
            btnSave.Location = new Point(186, 366);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(94, 29);
            btnSave.TabIndex = 12;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // btnCancel
            // 
            btnCancel.Location = new Point(517, 366);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(94, 29);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // btnShowPassword
            // 
            btnShowPassword.Location = new Point(517, 187);
            btnShowPassword.Name = "btnShowPassword";
            btnShowPassword.Size = new Size(43, 27);
            btnShowPassword.TabIndex = 14;
            btnShowPassword.Text = "S/H";
            btnShowPassword.UseVisualStyleBackColor = true;
            btnShowPassword.Click += btnShowPassword_Click;
            // 
            // frmMemberDetails
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnShowPassword);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Controls.Add(txtCountry);
            Controls.Add(txtEmail);
            Controls.Add(txtPassword);
            Controls.Add(txtCity);
            Controls.Add(txtMemberID);
            Controls.Add(txtMemberName);
            Controls.Add(lbCountry);
            Controls.Add(lbPassword);
            Controls.Add(lbEmail);
            Controls.Add(lbCity);
            Controls.Add(lbMemberName);
            Controls.Add(lbMemberID);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "frmMemberDetails";
            Text = "frmMemberDetails";
            Load += frmMemberDetails_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lbMemberID;
        private Label lbMemberName;
        private Label lbCity;
        private Label lbEmail;
        private Label lbPassword;
        private Label lbCountry;
        private TextBox txtMemberName;
        private TextBox txtMemberID;
        private TextBox txtCity;
        private TextBox txtPassword;
        private TextBox txtEmail;
        private TextBox txtCountry;
        private Button btnSave;
        private Button btnCancel;
        private Button btnShowPassword;
    }
}