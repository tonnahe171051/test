﻿using BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStoreWinApp
{
    public partial class frmMemberDetails : Form
    {
        public MemberObject memberObject { get; set; }
        public IMemberRepository MemberRepository = new MemberRepository();

        public bool InsertOrUpdate { get; set; }
        public bool ValidAdmin { get; set; }
        public frmMemberDetails()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmMemberDetails_Load(object sender, EventArgs e)
        {
            try
            {
                //authorization
                if (ValidAdmin)
                {
                    txtPassword.Enabled = false;
                    txtPassword.UseSystemPasswordChar = true;
                    btnShowPassword.Enabled = false;
                    txtMemberID.Enabled = false;
                    txtEmail.Enabled = false;
                }
                if (InsertOrUpdate == true)
                {
                    txtMemberID.Enabled = false;
                    txtMemberID.Text = memberObject.MemberID.ToString();
                    txtMemberName.Text = memberObject.MemberName.ToString();
                    txtPassword.Text = MemberRepository.GetMemberByID(memberObject.MemberID).Password;
                    txtPassword.UseSystemPasswordChar = true;
                    txtEmail.Text = memberObject.Email.ToString();
                    txtCity.Text = memberObject.City.ToString();
                    txtCountry.Text = memberObject.Country.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Invalid info");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (InsertOrUpdate == true)
                {
                    memberObject.MemberName = txtMemberName.Text;
                    memberObject.Email = txtEmail.Text;
                    memberObject.City = txtCity.Text;
                    memberObject.Country = txtCountry.Text;
                    memberObject.Password = txtPassword.Text;
                    MemberRepository.UpdateMember(memberObject);
                    MessageBox.Show("Successfully");
                    Close();
                }
                else if (InsertOrUpdate == false)
                {
                    var memb = new MemberObject
                    {
                        MemberID = int.Parse(txtMemberID.Text),
                        MemberName = txtMemberName.Text,
                        Email = txtEmail.Text,
                        City = txtCity.Text,
                        Country = txtCountry.Text,
                        Password = txtPassword.Text
                    };

                    MemberRepository.InsertMember(memb);
                    MessageBox.Show("Successfully");
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Invalid");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnShowPassword_Click(object sender, EventArgs e)
        {
            if (txtPassword.UseSystemPasswordChar)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }
    }
}
