﻿namespace MyStoreWinApp
{
    partial class frmMemberManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMemberManagement));
            lbSearch = new Label();
            lbMemberID = new Label();
            lbCountry = new Label();
            lbEmail = new Label();
            lbMemberName = new Label();
            lbCity = new Label();
            lbFilter = new Label();
            txtMemberName = new TextBox();
            txtEmail = new TextBox();
            txtMemberID = new TextBox();
            txtSearch = new TextBox();
            txtCity = new TextBox();
            txtCountry = new TextBox();
            dgvMemberList = new DataGridView();
            btnNew = new Button();
            btnUpdate = new Button();
            btnDelete = new Button();
            btnClose = new Button();
            cbFilter = new ComboBox();
            cbFilterList = new ComboBox();
            cbSearch = new ComboBox();
            txtDesc = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvMemberList).BeginInit();
            SuspendLayout();
            // 
            // lbSearch
            // 
            lbSearch.AutoSize = true;
            lbSearch.Location = new Point(50, 44);
            lbSearch.Name = "lbSearch";
            lbSearch.Size = new Size(53, 20);
            lbSearch.TabIndex = 0;
            lbSearch.Text = "Search";
            // 
            // lbMemberID
            // 
            lbMemberID.AutoSize = true;
            lbMemberID.Location = new Point(50, 113);
            lbMemberID.Name = "lbMemberID";
            lbMemberID.Size = new Size(84, 20);
            lbMemberID.TabIndex = 1;
            lbMemberID.Text = "Member ID";
            // 
            // lbCountry
            // 
            lbCountry.AutoSize = true;
            lbCountry.Location = new Point(475, 260);
            lbCountry.Name = "lbCountry";
            lbCountry.Size = new Size(60, 20);
            lbCountry.TabIndex = 2;
            lbCountry.Text = "Country";
            // 
            // lbEmail
            // 
            lbEmail.AutoSize = true;
            lbEmail.Location = new Point(50, 260);
            lbEmail.Name = "lbEmail";
            lbEmail.Size = new Size(46, 20);
            lbEmail.TabIndex = 3;
            lbEmail.Text = "Email";
            // 
            // lbMemberName
            // 
            lbMemberName.AutoSize = true;
            lbMemberName.Location = new Point(50, 183);
            lbMemberName.Name = "lbMemberName";
            lbMemberName.Size = new Size(109, 20);
            lbMemberName.TabIndex = 4;
            lbMemberName.Text = "Member Name";
            // 
            // lbCity
            // 
            lbCity.AutoSize = true;
            lbCity.Location = new Point(475, 183);
            lbCity.Name = "lbCity";
            lbCity.Size = new Size(34, 20);
            lbCity.TabIndex = 5;
            lbCity.Text = "City";
            // 
            // lbFilter
            // 
            lbFilter.AutoSize = true;
            lbFilter.Location = new Point(475, 113);
            lbFilter.Name = "lbFilter";
            lbFilter.Size = new Size(42, 20);
            lbFilter.TabIndex = 6;
            lbFilter.Text = "Filter";
            // 
            // txtMemberName
            // 
            txtMemberName.Location = new Point(165, 176);
            txtMemberName.Name = "txtMemberName";
            txtMemberName.Size = new Size(282, 27);
            txtMemberName.TabIndex = 7;
            // 
            // txtEmail
            // 
            txtEmail.Location = new Point(165, 253);
            txtEmail.Name = "txtEmail";
            txtEmail.ReadOnly = true;
            txtEmail.Size = new Size(282, 27);
            txtEmail.TabIndex = 8;
            // 
            // txtMemberID
            // 
            txtMemberID.Location = new Point(165, 106);
            txtMemberID.Name = "txtMemberID";
            txtMemberID.ReadOnly = true;
            txtMemberID.Size = new Size(282, 27);
            txtMemberID.TabIndex = 9;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(165, 41);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(282, 27);
            txtSearch.TabIndex = 10;
            txtSearch.TextChanged += txtSearch_TextChanged;
            // 
            // txtCity
            // 
            txtCity.Location = new Point(543, 176);
            txtCity.Name = "txtCity";
            txtCity.Size = new Size(282, 27);
            txtCity.TabIndex = 11;
            // 
            // txtCountry
            // 
            txtCountry.Location = new Point(543, 253);
            txtCountry.Name = "txtCountry";
            txtCountry.Size = new Size(282, 27);
            txtCountry.TabIndex = 12;
            // 
            // dgvMemberList
            // 
            dgvMemberList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvMemberList.Location = new Point(12, 347);
            dgvMemberList.Name = "dgvMemberList";
            dgvMemberList.RowHeadersWidth = 51;
            dgvMemberList.RowTemplate.Height = 29;
            dgvMemberList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvMemberList.Size = new Size(926, 159);
            dgvMemberList.TabIndex = 13;
            // 
            // btnNew
            // 
            btnNew.Location = new Point(172, 301);
            btnNew.Name = "btnNew";
            btnNew.Size = new Size(94, 29);
            btnNew.TabIndex = 14;
            btnNew.Text = "New";
            btnNew.UseVisualStyleBackColor = true;
            btnNew.Click += btnNew_Click;
            // 
            // btnUpdate
            // 
            btnUpdate.Location = new Point(441, 301);
            btnUpdate.Name = "btnUpdate";
            btnUpdate.Size = new Size(94, 29);
            btnUpdate.TabIndex = 15;
            btnUpdate.Text = "Update";
            btnUpdate.UseVisualStyleBackColor = true;
            btnUpdate.Click += btnUpdate_Click;
            // 
            // btnDelete
            // 
            btnDelete.Location = new Point(731, 301);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(94, 29);
            btnDelete.TabIndex = 16;
            btnDelete.Text = "Delete";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(441, 512);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(94, 29);
            btnClose.TabIndex = 17;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // cbFilter
            // 
            cbFilter.FormattingEnabled = true;
            cbFilter.Items.AddRange(new object[] { "City", "Country" });
            cbFilter.Location = new Point(543, 105);
            cbFilter.Name = "cbFilter";
            cbFilter.Size = new Size(129, 28);
            cbFilter.TabIndex = 18;
            cbFilter.SelectedIndexChanged += cbFilter_SelectedIndexChanged;
            // 
            // cbFilterList
            // 
            cbFilterList.FormattingEnabled = true;
            cbFilterList.Items.AddRange(new object[] { "All" });
            cbFilterList.Location = new Point(696, 105);
            cbFilterList.Name = "cbFilterList";
            cbFilterList.Size = new Size(129, 28);
            cbFilterList.TabIndex = 19;
            cbFilterList.SelectedIndexChanged += cbFilterList_SelectedIndexChanged;
            // 
            // cbSearch
            // 
            cbSearch.FormattingEnabled = true;
            cbSearch.Items.AddRange(new object[] { "ID", "Name" });
            cbSearch.Location = new Point(475, 41);
            cbSearch.Name = "cbSearch";
            cbSearch.Size = new Size(129, 28);
            cbSearch.TabIndex = 20;
            cbSearch.SelectedIndexChanged += cbSearch_SelectedIndexChanged;
            // 
            // txtDesc
            // 
            txtDesc.Location = new Point(626, 41);
            txtDesc.Name = "txtDesc";
            txtDesc.Size = new Size(129, 29);
            txtDesc.TabIndex = 21;
            txtDesc.Text = "DESC";
            txtDesc.UseVisualStyleBackColor = true;
            txtDesc.Click += txtDesc_Click;
            // 
            // frmMemberManagement
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(950, 550);
            Controls.Add(txtDesc);
            Controls.Add(cbSearch);
            Controls.Add(cbFilterList);
            Controls.Add(cbFilter);
            Controls.Add(btnClose);
            Controls.Add(btnDelete);
            Controls.Add(btnUpdate);
            Controls.Add(btnNew);
            Controls.Add(dgvMemberList);
            Controls.Add(txtCountry);
            Controls.Add(txtCity);
            Controls.Add(txtSearch);
            Controls.Add(txtMemberID);
            Controls.Add(txtEmail);
            Controls.Add(txtMemberName);
            Controls.Add(lbFilter);
            Controls.Add(lbCity);
            Controls.Add(lbMemberName);
            Controls.Add(lbEmail);
            Controls.Add(lbCountry);
            Controls.Add(lbMemberID);
            Controls.Add(lbSearch);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "frmMemberManagement";
            Text = "frmMemberManagement";
            Load += frmMemberManagement_Load;
            ((System.ComponentModel.ISupportInitialize)dgvMemberList).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lbSearch;
        private Label lbMemberID;
        private Label lbCountry;
        private Label lbEmail;
        private Label lbMemberName;
        private Label lbCity;
        private Label lbFilter;
        private TextBox txtMemberName;
        private TextBox txtEmail;
        private TextBox txtMemberID;
        private TextBox txtSearch;
        private TextBox txtCity;
        private TextBox txtCountry;
        private DataGridView dgvMemberList;
        private Button btnNew;
        private Button btnUpdate;
        private Button btnDelete;
        private Button btnClose;
        private ComboBox cbFilter;
        private ComboBox cbFilterList;
        private ComboBox cbSearch;
        private Button txtDesc;
    }
}