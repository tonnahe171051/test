﻿using BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStoreWinApp
{
    public partial class frmMemberManagement : Form
    {
        private List<MemberObject> membsStore;

        private List<MemberObject> memberStoreListPipe;
        private List<MemberObject> memberStoreListBackUp;
        public IMemberRepository memberRepository { get; set; }

        BindingSource source;
        public frmMemberManagement()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            frmMemberDetails frmMemberDetails = new frmMemberDetails
            {
                Text = "New member",
                InsertOrUpdate = false,
                MemberRepository = memberRepository
            };
            if (frmMemberDetails.ShowDialog() == DialogResult.OK)
            {
                LoadMembList();
                source.Position = source.Count - 1;
            }
            LoadMembList();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            frmMemberDetails frmMemberDetails = new frmMemberDetails
            {
                Text = "Update member",
                InsertOrUpdate = true,
                ValidAdmin = true,
                memberObject = GetMemberObject(),
                MemberRepository = memberRepository
            };
            if (frmMemberDetails.ShowDialog() == DialogResult.OK)
            {
                LoadMembList();
                source.Position = source.Count - 1;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var memb = GetMemberObject();
                memberRepository.DeleteMember(memb.MemberID);
                LoadMembList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Delete a member");
            }
        }
        private MemberObject GetMemberObject()
        {
            MemberObject memberObject = null;
            try
            {
                memberObject = new MemberObject
                {
                    MemberID = int.Parse(txtMemberID.Text),
                    MemberName = txtMemberName.Text,
                    Email = txtEmail.Text,
                    City = txtCity.Text,
                    Country = txtCountry.Text
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Member");
            }
            return memberObject;
        }
        private List<string> GetAllCity()
        {
            return memberRepository.GetListAllMember().Select(mb => mb.City).Distinct().ToList();
        }

        private List<string> GetAllCountry()
        {
            return memberRepository.GetListAllMember().Select(mb => mb.Country).Distinct().ToList();
        }

        private void ClearText()
        {
            txtMemberID.Text = String.Empty;
            txtMemberName.Text = String.Empty;
            txtCity.Text = String.Empty;
            txtCountry.Text = String.Empty;
            txtEmail.Text = String.Empty;
        }
        //load data for filter and desc/asc
        public void LoadListPipe(List<MemberObject> listMb)
        {
            source = new BindingSource();
            source.DataSource = listMb;

            txtMemberID.DataBindings.Clear();
            txtMemberName.DataBindings.Clear();
            txtCity.DataBindings.Clear();
            txtCountry.DataBindings.Clear();
            txtEmail.DataBindings.Clear();

            txtMemberID.DataBindings.Add("Text", source, "MemberID");
            txtMemberName.DataBindings.Add("Text", source, "MemberName");
            txtCity.DataBindings.Add("Text", source, "City");
            txtCountry.DataBindings.Add("Text", source, "Country");
            txtEmail.DataBindings.Add("Text", source, "Email");

            dgvMemberList.DataSource = null;
            dgvMemberList.DataSource = source;

            if (listMb.Count() == 0)
            {
                ClearText();
                btnDelete.Enabled = false;
                btnNew.Enabled = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnDelete.Enabled = true;
                btnNew.Enabled = true;
                btnUpdate.Enabled = true;
            }
        }

        //load data for show
        public void LoadMembList()
        {
            var membs = memberRepository.GetListAllMember();
            membsStore = membs;
            try
            {
                source = new BindingSource();
                source.DataSource = membs;

                txtMemberID.DataBindings.Clear();
                txtMemberName.DataBindings.Clear();
                txtCity.DataBindings.Clear();
                txtCountry.DataBindings.Clear();
                txtEmail.DataBindings.Clear();

                txtMemberID.DataBindings.Add("Text", source, "MemberID");
                txtMemberName.DataBindings.Add("Text", source, "MemberName");
                txtCity.DataBindings.Add("Text", source, "City");
                txtCountry.DataBindings.Add("Text", source, "Country");
                txtEmail.DataBindings.Add("Text", source, "Email");

                dgvMemberList.DataSource = null;
                dgvMemberList.DataSource = source;

                if (membs.Count() == 0)
                {
                    ClearText();
                    btnDelete.Enabled = false;
                    btnNew.Enabled = false;
                    btnUpdate.Enabled = false;
                }
                else
                {
                    btnDelete.Enabled = true;
                    btnNew.Enabled = true;
                    btnUpdate.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load Member list");
            }

        }

        private void frmMemberManagement_Load(object sender, EventArgs e)
        {
            cbSearch.SelectedIndex = 0;
            cbFilter.SelectedIndex = 0;
            cbFilterList.SelectedIndex = 0;
            LoadMembList();
            dgvMemberList.CellDoubleClick += DgvMemberList_CellDoubleClick;
            dgvMemberList.CellFormatting += DgvMemberList_CellFormatting;
        }

        //format password to password char
        private void DgvMemberList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (dgvMemberList.Columns[e.ColumnIndex].Name == "Password")
            {

                if (e.Value != null)
                {
                    e.Value = new string('●', e.Value.ToString().Length);
                }
            }
        }

        private void DgvMemberList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmMemberDetails frmMemberDetails = new frmMemberDetails
            {
                Text = "Update members",
                InsertOrUpdate = true,
                ValidAdmin = true,
                memberObject = GetMemberObject(),
                MemberRepository = memberRepository
            };
            if (frmMemberDetails.ShowDialog() == DialogResult.OK)
            {
                LoadMembList();
                source.Position = source.Count - 1;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string searchChar = txtSearch.Text;
                if (cbSearch.SelectedIndex == 0)
                {
                    if (string.IsNullOrWhiteSpace(searchChar))
                    {
                        //LoadMembList();
                        memberStoreListPipe = memberRepository.GetListAllMember();
                        LoadListPipe(memberStoreListPipe);
                    }
                    else
                    {
                        memberStoreListPipe = memberRepository.searchMemberById(int.Parse(searchChar), null);
                        LoadListPipe(memberStoreListPipe);
                    }

                }
                else
                {
                    if (string.IsNullOrWhiteSpace(searchChar))
                    {
                        //LoadMembList();
                        memberStoreListPipe = memberRepository.GetListAllMember();
                        LoadListPipe(memberStoreListPipe);
                    }
                    else
                    {
                        memberStoreListPipe = memberRepository.searchMemberByName(searchChar, null);
                        LoadListPipe(memberStoreListPipe);
                    }
                }
                memberStoreListBackUp = memberStoreListPipe;
            }
            catch (Exception ex)
            {
            };
        }



        private void cbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> listCC = null;

            switch (cbFilter.SelectedIndex)
            {
                case 0:
                    {
                        listCC = GetAllCity();
                        break;
                    }
                case 1:
                    {
                        listCC = GetAllCountry();
                        break;
                    }
            }
            cbFilterList.Items.Clear();
            if (listCC != null && listCC.Any())
            {
                cbFilterList.Items.AddRange(listCC.ToArray());
                cbFilterList.SelectedIndex = 0; // Gán SelectedIndex khi danh sách có phần tử
            }
            else
            {
                cbFilterList.SelectedIndex = -1; // Gán SelectedIndex = -1 khi danh sách rỗng
            }
        }

        int count = 0;

        private void cbFilterList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFilterList.SelectedItem.ToString() == "Ha Noi" && count == 0)
            {
                memberStoreListPipe = memberRepository.GetListAllMember();
                ++count;
            }
            else if (count != 0)
            {
                memberStoreListPipe = memberStoreListBackUp;
                if (cbFilter.SelectedIndex == 0)
                {
                    memberStoreListPipe = memberRepository.filterByCity(cbFilterList.SelectedItem.ToString(), memberStoreListPipe);
                    LoadListPipe(memberStoreListPipe);
                }
                else
                {
                    memberStoreListPipe = memberRepository.filterByCountry(cbFilterList.SelectedItem.ToString(), memberStoreListPipe);
                    LoadListPipe(memberStoreListPipe);
                }
            }
        }

        private void txtDesc_Click(object sender, EventArgs e)
        {
            if (txtDesc.Text.Equals("DESC"))
            {
                if (memberStoreListPipe == null)
                {

                    var membs = memberRepository.GetListAllMember();
                    LoadListPipe(membs.OrderByDescending(x => x.MemberName).ToList());
                    txtDesc.Text = "ASC";
                }
                else
                {
                    LoadListPipe(memberStoreListPipe.OrderByDescending(x => x.MemberName).ToList());
                    txtDesc.Text = "ASC";
                }

            }
            else if (txtDesc.Text.Equals("ASC"))
            {
                if (memberStoreListPipe == null)
                {
                    var membs = memberRepository.GetListAllMember();
                    LoadListPipe(membs.OrderBy(x => x.MemberName).ToList());
                    txtDesc.Text = "DESC";
                }
                else
                {
                    LoadListPipe(memberStoreListPipe.OrderBy(x => x.MemberName).ToList());
                    txtDesc.Text = "DESC";
                }
            }
        }

        private void cbSearch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
