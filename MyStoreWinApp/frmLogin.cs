﻿using BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStoreWinApp
{
    public partial class frmLogin : Form
    {
        public IMemberRepository MemberRepository = new MemberRepository();
        public MemberObject memberObject { get; set; }
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string email = txtEmail.Text.Trim();
            string password = txtPassword.Text.Trim();
            memberObject = MemberRepository.GetMemberByEmailPassword(email, password);
            if (memberObject != null && email.Equals("admin@fstore.com"))
            {
                frmMemberManagement memberManagement = new frmMemberManagement();
                memberManagement.memberRepository = MemberRepository;
                this.Hide();
                memberManagement.ShowDialog();
                this.Show();
            }
            else if (memberObject != null)
            {
                frmMemberDetails memberDetails = new frmMemberDetails
                {
                    memberObject = memberObject,
                    InsertOrUpdate = true
                };

                this.Hide();
                memberDetails.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("Invalid info!");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
